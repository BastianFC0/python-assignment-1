# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 17:43:18 2022

@author: Bastian FC
"""
import csv

def get_dictionary_list(path):
    fetched = open(path)
    data = fetched.readlines()
    headerString = data.pop(0).split(",")
    finishedList = []
    for i in data:
        index = data.index(i)
        rawString = data[index]
        lineHolder = rawString.split(",")
        lengthHolderString = "{headerString[0]:lineHolder[0]"
        for i in range(1, len(headerString)):
            lengthHolderString += ", headerString[{}]:lineHolder[{}]".format(i,i)
        lengthHolderString += "}"
        exec("finishedList.append({})".format(lengthHolderString))
    return finishedList

def write_dictionary_list(path, dictionary_list):
    writer = csv.writer(open(path,'w', newline=''))
    firstRow = list(dictionary_list[0].keys())
    headerHolderString = "firstRow[0]"
    for i in range(1, len(firstRow)):
        headerHolderString += ", firstRow[{}]".format(i)
    exec("writer.writerow([{}])".format(headerHolderString))
    for i in dictionary_list:
        columnsHolderString = "i[firstRow[0]]"
        for j in range(1, len(i)):
            columnsHolderString += ", i[firstRow[{}]]".format(j)
        exec("writer.writerow([{}])".format(columnsHolderString))