# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 19:18:36 2022

@author: Bastian FC
"""

import copy

from operator import attrgetter

class Grader():
    
    def __init__(self, studentList, key):
        self.__students = studentList
        self.__correct_key = key
        self.__corrected_scores = []
        self.__sorted_scores = []
        
    def compute_grade(self, score):
        if 100 >= score >= 90:
            letterGrade = "A"
        elif 89 >= score >= 80:
            letterGrade = "B"
        elif 79 >= score >= 70:
            letterGrade = "C"
        elif 69 >= score >= 60:
            letterGrade = "D"
        else:
            letterGrade = "F"
        return letterGrade
    
    def compute_score(self, dictionary):
        score = 0
        keyList = [i for i in self.__correct_key]
        answerString = dictionary['answer\n']
        answerList = [i for i in answerString]
        for i in range(0,10):
            if keyList[i] == answerList[i]:
                score += 10
        return score
    
    def correct_students(self):
        deepCopy = copy.deepcopy(self.__students)
        for i in deepCopy:
            newScore = Grader.compute_score(self,i)
            newGrade = Grader.compute_grade(self,newScore)
            i.pop("answer\n")
            i["score"] = newScore 
            i["grade"] = newGrade
        self.__corrected_scores = deepCopy
        self.__sorted_scores = sorted(deepCopy, key = lambda x: x["score"], reverse = True)
    
    def get_corrected_scores(self):
        return self.__corrected_scores
        
    def get_sorted_scores(self):
        return self.__sorted_scores
        